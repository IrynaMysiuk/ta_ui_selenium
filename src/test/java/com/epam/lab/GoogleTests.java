package com.epam.lab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

public class GoogleTests {
    private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(GoogleTests.class);
    WebDriver driver;

    public GoogleTests() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
    }

    @Test(description = "Verify search Apple in Google")
    public void checkGoogle() {
        driver.get("https://www.google.com");
        WebElement inputField = driver.findElement(By.name("q"));
        inputField.sendKeys("Apple");
        inputField.submit();
        log.info("Page title is:" + driver.getTitle());
        (new WebDriverWait(driver, 50)).until((dr) -> dr.getTitle().startsWith("Apple"));
        Assert.assertTrue(driver.getTitle().contains("Apple"), "Title does not contain Apple word");
        WebElement imageTab = driver.findElement(By.xpath("//div[@id='hdtb-msb-vis']/div[2]/a"));
        imageTab.click();
        WebElement imageButton = driver.findElement(By.xpath("//*[@aria-label=\"airpods pro\"]"));
        Assert.assertTrue(imageButton.isDisplayed(), "This element is not displayed");

    }

    @AfterMethod
    public void closeDriver() {
        driver.quit();
    }
}
